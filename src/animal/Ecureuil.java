package animal;

public class Ecureuil {
    private int noisette;
    private int pointDeVie;

    public Ecureuil(int vie, int noisette) {
        this.pointDeVie = 5;
        this.noisette = noisette;
    }

    public int stockerNourriture(int noisette) {
        this.noisette = this.noisette + noisette;
        return this.noisette;
    }

    public boolean etreMort() {
        boolean mort = false;
        if(this.passerUnjour() == true){
            if(this.noisette==0) {
                this.pointDeVie = this.pointDeVie - 2;
            }
            this.noisette = this.noisette - 1;
        }
        if(this.pointDeVie == 0) {
            mort = true;
        }
        return mort;
    }

    public int getPV() {
        int pv = this.pointDeVie;
        return pv;
    }

    public int getNoisette() {
        int nourriture = this.noisette;
        return nourriture;
    }


    public boolean passerUnjour() {
        boolean res = true;
        if(pointDeVie == 0 || this.etreMort() == true) {
            res = false;
        }
        return res;
    }

    public String toString() {
        System.out.println("Ecureuil - pv:" + this.pointDeVie + " noisette:" + this.noisette);
		return null;
    }

}