package animal;

public class Loup {
    private int viande;
    private int pointDeVie;

    public Loup(int vie, int viande) {
        this.pointDeVie = 30;
        this.viande = viande;
    }

    public int stockerNourriture(int viande) {
        this.viande = this.viande + viande;
        return this.viande;
    }

    public boolean etreMort() {
        boolean mort = false;
        if(this.passerUnjour() == true){
            if(this.viande==0) {
                this.pointDeVie = this.pointDeVie - 4;
            }
            this.viande = this.viande/2;
        }
        if(this.pointDeVie == 0) {
            mort = true;
        }
        return mort;
    }

    public int getPV() {
        int pv = this.pointDeVie;
        return pv;
    }

    public int getViande() {
        int nourriture = this.viande;
        return nourriture;
    }


    public boolean passerUnjour() {
        boolean res = true;
        if(pointDeVie == 0 || this.etreMort() == true) {
            res = false;
        }
        return res;
    }

    public String toString() {
        System.out.println("Loup - pv:" + this.pointDeVie + " viande:" + this.viande);
		return null;
    }
}